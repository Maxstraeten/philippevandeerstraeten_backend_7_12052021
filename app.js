'use strict'
const dotenv = require('dotenv')
const express = require('express')
const cors = require('cors')
const favicon = require('serve-favicon')
const serveStatic = require('serve-static')
const fs = require('fs')
const morgan = require('morgan')
const path = require('path')

const Sequelize = require('sequelize')
const db = new Sequelize('groupomania', 'mysql', 'philippe', {
	host: 'localhost',
	dialect: 'mysql',
	// operatorsAliases: false,

	pool: {
		max: 5,
		min: 0,
		acquire: 30000,
		idle: 10000,
	},
})

//Test DB
db.authenticate()
	.then(() => console.log('Database connected...'))
	.catch((err) => console.log('Error: ' + err))

dotenv.config()

const app = express()
const port = process.env.PORT || 3000

//favicon
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')))
// Morgan gestion des erreurs dans le terminal
app.use(
	morgan('dev', {
		skip: (req, res) => {
			return res.statusCode < 400
		},
	})
)
// Morgan création de la page log
app.use(
	morgan('common', {
		stream: fs.createWriteStream(path.join(__dirname, 'log/access.log'), {
			flags: 'a',
		}),
	})
)
// cors-js
app.use(cors())
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader(
		'Access-Control-Allow-Headers',
		'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'
	)
	res.setHeader(
		'Access-Control-Allow-Methods',
		'GET, POST, PUT, DELETE, PATCH, OPTIONS'
	)
	next()
})

// body-parser à partir de la version expressjs 4.16
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// lien avec les fichiers statiques
app.use(serveStatic(path.join(__dirname, 'public')))

// mise en place de pug
app.set('view engine', 'pug')
app.get('/', (req, res) => res.send('Hello there !'))

app.post('/user', (req, res) => {
	let msg = req.body
	console.log(msg)
	res.status(201).json(msg)
})

app.listen(port, () =>
	console.log(`Server listening at http://localhost: ${port}`)
)

module.exports = app
